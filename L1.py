import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

# m = int(input("Podaj liczbę wierszy: "))
# n = int(input("Podaj liczbę kolumn: "))
# oceny = np.arange(2.0, 5.6, 0.5)
# oceny = np.random.choice(oceny, size=(m, n))
# print(oceny)
# #oceny = pd.DataFrame(oceny, columns=[f"Przedmiot {i+1}" for i in range(n)])

# #Zadanie 1a
# niezal = np.sum(oceny < 3.0, axis=1)
# print(niezal)
# wynik = np.sum(niezal >= n)
# print("Zadanie 1a:", wynik)


# #Zadanie 1b
# srednie = np.mean(oceny, axis=1)
# print("Zadanie 1b max wartosc:", round(max(srednie), 2))
# print("Zadanie 1b min wartosc:", round(min(srednie), 2))

# #Zadanie 1c
# czy_float = np.max(oceny, axis=0)  #wyszukanie max ocen dla kolumn
# ile_match = np.sum(oceny == czy_float, axis=1) #spr ile matchuje dla danego studenta
# wynik = np.argmax(ile_match) #spr gdzie najwiecej matchuje
# #print(czy_float, ile_match, wynik, "\n")
# print("Zadanie 1c:", oceny[wynik])

# #Zadanie 1d
# for przedmiot in range(n):
#     oceny_przedmiotu = oceny[:, przedmiot]
#     hist, bins = np.histogram(oceny_przedmiotu, bins=10, range=(2.0, 5.5))
#     plt.figure()
#     plt.hist(oceny_przedmiotu, bins=bins, edgecolor='black')
#     plt.title(f"Histogram ocen przedmiotu {przedmiot + 1}")
#     plt.xlabel("Oceny")
#     plt.ylabel("Liczba studentów")
#     plt.grid(True)
#     plt.show()

# #Zadanie 1e
# srednie = np.mean(oceny, axis=1)
# #print(srednie, "\n")
# pom1 = np.where(srednie >= 4.5)[0]
# wynik2 = pom1 + 1
# print("Zadanie 1e:", wynik2)


# #Zadanie 2
# m = int(input("Podaj liczbę wierszy: "))
# n = int(input("Podaj liczbę kolumn: "))
# cos = np.arange(0, 1000)
# m1 = np.random.choice(cos, size=(m, n))
# m2 = np.random.choice(cos, size=(m, n))
# print(m1)
# print(m2)

# wynik = 0
# for i in range(m):
#     for j in range(n):
#         wynik += abs(m1[i][j]-m2[i][j])

# print(wynik)


# #Zadanie 3
# n = int(input("Podaj liczbę wierszy: "))
# m = n+1
# # cos = np.arange(0, 100)
# # matrix = np.random.choice(cos, size=(m, n))
# matrix = np.random.random_integers(1,30,(n,m))
# matrix = matrix.astype(float)

# for i in range(n):
#     if i >= m or i >= n: 
#         break
#     a=1

#     while matrix[i].any() == 0:
#         matrix = np.delete(matrix,i,0)
#         n-=1

#     while matrix[:,i].any() == 0:
#         matrix = np.delete(matrix,i,1)
#         m-=1

#     while matrix[i,i] == 0:
#         matrix[[i,i+a]] = matrix[[i+a,i]]
#         a+=1

#     matrix[i]/=matrix[i,i]
#     for j in range(i+1,n):
#         matrix[j] -= matrix[j,i]*matrix[i]

# print(matrix)

#Zadanie 4
#Przyjąć, że pierwsza macierz przedstawia paragon w sklepie i zawiera w
#kolumnach: numer klienta, numer towaru, liczbę sztuk (lub wagę w kilogramach).

#Druga macierz Zawiera opisy towarów, tj.: numer towaru, cenę jednostkową (lub za
#kg), informację czy towar jest sprzedawany na sztuki czy na wagę.

paragon = np.array([[2, 6, 1], [4, 1, 3.5], [1, 12, 3.5]])
opisyTowarow = np.array([[6, 30, "na sztuki"], [1, 15, "na wage"], [12, 4, "na sztuki"]])

czy_float = True
numery_towarow = set(wiersz[0] for wiersz in opisyTowarow)  #wydzielenie wiersza z nr towarów
sumy = {}
for wiersz in paragon:
    numer_towaru = int(wiersz[1])
    numer_klienta = wiersz[0]

    if str(numer_towaru) not in numery_towarow:
        czy_float = False

    for wiersz_towary in opisyTowarow:
        if str(numer_towaru) == wiersz_towary[0]:
            if wiersz_towary[2] == 'na sztuki' and wiersz[2] != int(wiersz[2]):
                czy_float = False

            if numer_klienta in sumy.keys():
                sumy[numer_klienta] += float(wiersz[2]) * float(wiersz_towary[1])
            else:
                sumy[numer_klienta] = float(wiersz[2]) * float(wiersz_towary[1])

if czy_float:
    print('all good amigos', sumy)
else:
    print('Error 404, coś nie tak')